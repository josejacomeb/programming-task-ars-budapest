# Programming Task ARS Budapest



## Tasks

**Python**

- [x] ~~Task 1 - Explore the dataset~~
- [x] ~~Task 2 - Prepare for training~~
- [x] ~~Task 3 - Convert boxes from absolute to relative coordinates~~

**C++**

- [X] ~~Task 1 - Implement the constructor~~
- [X] ~~Task 2 - Calculate the IoU of two boxes~~
- [X] ~~Task 3 - Merge the new detections to the existing tracks~~
