#include <vector>
#include <opencv2/opencv.hpp>

#include <algorithm>    //Include additional functions
#include <iostream>
#include <math.h>

class Box
{
private:
    float MidX;
    float MidY;
    float Width;
    float Height;
    // Note: you are free to add new methods and member variables.
    // You are also allowed to use OpenCV.
public:
    Box(int Left, int Top, int Width, int Height, int FrameWidth, int FrameHeight);

    float getIoU(Box other);
    //Test variables
    float get_MidX();
    float get_MidY();
    float get_Width();
    float get_Height();

};

// Task 1: Implement the constructor!
//   * It receives the position and size of the box in pixels.
//   * It also receives the size of the whole frame.
//   * Your task is to convert this to a relative size/position,
//     which is invariant to the frame size (as long as the aspect ratio is the same)

Box::Box(int Left, int Top, int Width, int Height, int FrameWidth, int FrameHeight)
{
    // Proposed solution
    /*
    * Explaination: Use the this keyword to point to the right variable, because the Height and Width have the same variable name
    */
    this -> MidX = (float) (Left + Width/2.0f) / FrameWidth;
    this -> MidY = (float) (Top + Height/2.0f) / FrameHeight;
    this -> Width = (float) Width/FrameWidth;
    this -> Height = (float) Height / FrameHeight;
}

// Task 2: Calculate the IoU of two boxes!
//   * The area of the intersection divided by the area of the union of the two boxes
//     is a good metric for matching boxes that belong to the same object
//   * Your task is to implement this

float Box::getIoU(Box Other)
{
    // Proposed solution
    /*
    * Explaination: Calculate the intersection Area and the Union Area and divide it, to get the IoU
    */
    float IoU, xA, yA, xB, yB, boxAArea, boxBArea, interArea;

    //Based in https://www.pyimagesearch.com/2016/11/07/intersection-over-union-iou-for-object-detection/
    xA = std::max((MidX - Width/2), (Other.MidX - Other.Width/2) ); //Top
    yA = std::max((MidY - Height/2), (Other.MidY - Other.Height/2)); //Left
    xB = std::min((MidX + Width/2), (Other.MidX + Other.Width/2) ); //Bottom
    yB = std::min((MidY + Height/2), (Other.MidY + Other.Height/2)); //Right
    interArea = std::max(0.0f, (xB - xA)) * std::max(0.0f, (yB - yA));
    boxAArea = Height * Width;
    boxBArea = Other.Height * Other.Width;
    IoU = interArea / (boxAArea + boxBArea - interArea);
    return IoU;
}

// Task 3: Merge the new detections to the existing tracks!
//   * We have a detection module that can detect vehicles. It takes a long time to execute.
//   * Between detections, a tracking module updates the bounding boxes so that they
//     follow the given vehicle
//   * Your task is to match the new detections with the existing tracks

std::vector<Box> mergeBoxes(std::vector<Box> TrackBoxes, std::vector<Box> DetectionBoxes)
{
    std::vector<Box> MergedBoxes;

    // Proposed solution
    /*
    * Explaination: Use two criteria to match the TrackBoxes with the detections ones,
    * first: use the euclidean distance between the centers of the Box objects
    * second: calculate the IoU between the two
    * To have a succesful match, my consideration is the IoU should be at least 1/3 and
    * the min_euclidean_distance might be less than the half of the shortest side (considering the objects can move fast)
    * Finally, I saved the matched detection's idxs to check if the box has been matched and to suggest to implement new tracks
    */
    float threshold_distance, threshold_IoU;
    float euclidean_distance_centers, boxesIoU;
    int min_idx = 100;
    float max_IoU, min_euclidean_distance;
    int exist_idx_in_matched_detections;
    std::vector<int> matched_detections;
    for(unsigned int i = 0; i < (int) TrackBoxes.size(); i++){
        //Check absolute euclidean distance and IoU
        max_IoU = 0;
        min_euclidean_distance = 10000;
        //Half of the distance of the shortest side is the min threshold for distance
        threshold_distance = 0.5 * std::min(TrackBoxes.at(i).get_MidX(), TrackBoxes.at(i).get_MidY());
        threshold_IoU = 0.33333; //Min overlapping is 1/3 of the
        for(unsigned int j = 0; j < (int) DetectionBoxes.size(); j++){
            euclidean_distance_centers = sqrt(pow((TrackBoxes.at(i).get_MidX() - DetectionBoxes.at(j).get_MidX()), 2) +
                                              pow((TrackBoxes.at(i).get_MidY() - DetectionBoxes.at(j).get_MidY()), 2));
            boxesIoU = TrackBoxes.at(i).getIoU(DetectionBoxes.at(j));
            if(euclidean_distance_centers <= min_euclidean_distance && boxesIoU >= max_IoU){
                min_idx = j;
                max_IoU = boxesIoU;
                min_euclidean_distance = euclidean_distance_centers;
            }
        }
        //Check for if the best candidate meets with the threshold conditions
        exist_idx_in_matched_detections = std::count(matched_detections.begin(), matched_detections.end(), min_idx);
        if (max_IoU >= threshold_IoU && min_euclidean_distance <= threshold_distance &&  exist_idx_in_matched_detections == 0){
            std::cout << "Track at: " << i << " matches with Detection at: " << min_idx << " Adding to the MergedBoxes vector! , IoU: "
                      << max_IoU << " euclidean_distance_centers: " << min_euclidean_distance << std::endl;
            MergedBoxes.push_back(DetectionBoxes.at(min_idx)); //Adding candidate
            matched_detections.push_back(min_idx);
        }
        else{
            std::cout << "Track at: " << i << " Couldn't be matched with any detection, best match: Detection idx: "
                      << min_idx <<  " IoU: " << max_IoU << " euclidean_distance_centers: " << min_euclidean_distance << std::endl;
        }
    }
    if(DetectionBoxes.size() > matched_detections.size()){
        std::cout << "Suggestion: Please consider add the following detections idx(s) to a new track: \n";
        for(unsigned int i = 0; i < (int) DetectionBoxes.size(); i++){
            exist_idx_in_matched_detections = std::count(matched_detections.begin(), matched_detections.end(), i);
            if (exist_idx_in_matched_detections == 0) std::cout << "\t" << i << std::endl;

        }
    }
    return MergedBoxes;
}

//Variables to
float Box::get_MidX(){
    return this->MidX;
}

float Box::get_MidY(){
    return this->MidY;
}
float Box::get_Height(){
    return this->Height;
}

float Box::get_Width(){
    return this->Width;
}

