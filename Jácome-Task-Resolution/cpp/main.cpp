#include <iostream>
#include <vector>

#include "coding-exercises.cpp"
using namespace std;

int main(){
    Box original_box = Box(200, 225, 400, 150, 800, 600);
    Box overlap_box = Box(200, 225, 400, 150, 800, 600);
    Box no_overlap_box = Box(0, 0, 200, 225, 800, 600);
    Box half_overlap_box = Box(400, 225, 400, 150, 800, 600);
    Box new_no_overlap_box = Box(0, 0, 20, 30, 800, 600);
    cout << "Bbox relative box: X: " << original_box.get_MidX() << " Y: "
              << original_box.get_MidY() << " Width: " << original_box.get_Width()
              << " Height: " << original_box.get_Height() << endl;
    cout << "IoU overlap box: " << original_box.getIoU(overlap_box) << endl;
    cout << "IoU no overlap box: " << original_box.getIoU(no_overlap_box) << endl;
    cout << "IoU half overlap box: " << original_box.getIoU(half_overlap_box) << endl;

    vector<Box> Tracks = {original_box, overlap_box, no_overlap_box};
    vector<Box> Detections = {original_box, new_no_overlap_box, no_overlap_box, half_overlap_box};
    vector<Box> Tracks_new = {original_box, half_overlap_box};

    vector<Box> MergedBoxes;

    MergedBoxes = mergeBoxes(Tracks, Detections);
    cout << "Merged boxes size: " << MergedBoxes.size() << endl;

    MergedBoxes = mergeBoxes(Tracks_new, Detections);
    cout << "Merged boxes size: " << MergedBoxes.size() << endl;
    return 0;
}
