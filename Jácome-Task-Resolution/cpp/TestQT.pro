TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        coding-exercises.cpp \
        main.cpp

HEADERS +=

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += opencv4
