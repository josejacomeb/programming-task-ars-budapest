# %%
# Task 1: Explore the dataset!
#   * Gather information about the dataset we have
#   * Bonus points for observations about how they'll affect training,
#     how to mitigate possible issues


# %%
import random
import os
import string
import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

%matplotlib inline

os.listdir('../../data/mini-coco')

img = cv2.imread('../../data/mini-coco/sedan/001956_I_5122630_1_190626_170302_105_0561.jpg.jpg')
plt.imshow(img)





# %%
# Task 2: Prepare for training!
#   * Prepare the dataset for an exploratory training attempt
#   * Bonus points for observations about usable ConvNet models,
#     evaluation methods, public data sources


# %%
def split_dataset(all_samples):
    """
    """
    # TODO: Implement this!
    training = all_samples[0:100]
    validation = all_samples[0:100]
    testing = all_samples[0:100]

    return training, validation, testing


# %%
# This cell will be used to check your split_dataset() function
all_samples = []
for i in range(100000):
    object_class = random.choice(['van', 'sedan', 'bus', 'truck'])
    box = {'x': random.randint(0, 1000),
           'y': random.randint(0, 1000),
           'w': random.randint(0, 1000),
           'h': random.randint(0, 1000)}
    box = frozenset(box.items())
    lp_alphabet = string.ascii_uppercase + string.digits
    lp_text = ''.join([random.choice(lp_alphabet) for i in range(6)])
    sample = {'img': f'./data/mini-coco/{object_class}/{i}.jpg',
              'class': object_class,
              'box': box,
              'lp_text': lp_text}
    sample = frozenset(sample.items())

    all_samples.append(sample)

training, validation, testing = [set(s) for s in split_dataset(all_samples)]

expected_size = len(all_samples)
actual_size = len(training) + len(validation) + len(testing)
assert actual_size == expected_size, \
    f'Subsets should have {expected_size} samples, actually have {actual_size}'

duplicates = training & validation
duplicates.union(training & testing)
duplicates.union(validation & testing)
assert len(duplicates) == 0, \
    'At least one sample appears in multiple subsets'

# %%
# Task 3: Convert boxes from absolute to relative coordinates!
#   * Your function receives the size and position of a box in pixels
#   * The returned box should be valid even if the frame is resized
#     (with the same aspect ratio)

# %%
def absolute_to_relative(absolute_box, frame_size):
    """
    """
    # TODO: Implement this!
    relative_box = {'x': 0,
                    'y': 0,
                    'height': 0,
                    'width': 0}

    return relative_box


# %%
# This cell will be used to check your absolute_to_relative() function

frame_size = {'width': 800,
              'height': 600}

absolute_box = {'top': 225,
                'left': 200,
                'height': 150,
                'width': 400}

# The expected result
# Note that the coordinates point to the middle of the box
expected_box = {'x': 0.5,
                'y': 0.5,
                'height': 0.25,
                'width': 0.5}

result_box = absolute_to_relative(absolute_box, frame_size)
assert result_box == expected_box, \
    f'{result_box} != {expected_box}'

# %%
